from django.urls import path
from . import views

app_name = 'base'

urlpatterns = [
    path('register/', views.userregister, name='userregister'),
    path('login/', views.userlogin, name='userlogin'),
    path('logout/', views.userlogout, name='userlogout'),
]
