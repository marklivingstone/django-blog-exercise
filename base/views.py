from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from .forms import UserRegistrationForm

# Create your views here.


def userregister(request):
    form = UserRegistrationForm()

    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('base:userlogin')

    context = {
        'form': form
    }
    return render(request, 'base/userregister.html', context)


def userlogin(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('blog:getblogs')
        else:
            messages.info(request, 'Username or Password is Incorrect')

    context = {}
    return render(request, 'base/userlogin.html', context)


def userlogout(request):

    logout(request)
    return redirect('base:userlogin')