from django.shortcuts import render, redirect
from .models import User, UserProfile
from .forms import UserForm, UserProfileForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages

# Create your views here.


def getUser(request):
    template = 'account/userprofile.html'

    if (request.user.id):
        post_data = request.POST or None

        current_user = request.user
        user_profile = request.user.userprofile

        user_form = UserForm(post_data, instance=current_user)
        profile_form = UserProfileForm(
            post_data, instance=user_profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.info(request, "User profile has been successfully updated!")
            return redirect('account:getuser')

        context = {
            'user': current_user,
            'user_profile': user_profile,
            'user_form': user_form,
            'profile_form': profile_form,
        }
        return render(request, template, context)
    else:
        return redirect('blog:getblogs')
