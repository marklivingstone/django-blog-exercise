from django.urls import path
from . import views
from django.urls import reverse_lazy
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView

app_name = 'account'

urlpatterns = [
    path('user_profile/', views.getUser, name='getuser'),
    path('change_password/', PasswordChangeView.as_view(success_url=reverse_lazy('account:change_password_complete'),
                                                        template_name='account/changepassword.html'), name='change_password'),
    path('change_password/complete/', PasswordChangeDoneView.as_view(template_name='account/changepassword_complete.html'),
         name='change_password_complete'),
]
