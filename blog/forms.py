from django import forms
from .models import Blog, Comment


class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ('title', 'text', 'user')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'text': forms.Textarea(attrs={'class': 'form-control'}),
            'user': forms.TextInput(attrs={'class': 'form-control', 'value': '', 'id': 'user', 'type': 'hidden'})
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('comment', 'blog', 'user')

        widgets = {
            'comment': forms.Textarea(attrs={'class': 'form-control', 'required': 'true'}),
            'user': forms.TextInput(attrs={'class': 'form-control', 'value': '', 'id': 'comment_user_id', 'type': 'hidden'}),
            'blog': forms.TextInput(attrs={'class': 'form-control', 'value': '', 'id': 'comment_blog_id', 'type': 'hidden'})
        }
