from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Blog, Comment
from .forms import BlogForm, CommentForm

# Create your views here.


def getBlogs(request):
    template = 'blog/blogs.html'
    blogs = Blog.objects.all()
    context = {
        'blogs': blogs
    }
    return render(request, template, context)


def getBlog(request, pk):
    template = 'blog/blog.html'
    blog = Blog.objects.get(pk=pk)
    comments = Comment.objects.filter(blog_id=pk)
    comment_form = CommentForm(request.POST or None, prefix='banned')

    if comment_form.is_valid():
        comment_form.save()
        return redirect('blog:getblog', pk=pk)

    context = {
        'blog': blog,
        'comment_form': comment_form,
        'comments': comments
    }
    return render(request, template, context)


def createBlog(request):
    template = 'blog/addblog.html'
    form = BlogForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('blog:getblogs')
    
    if (request.user.is_authenticated):
        context = {
            'form': form
        }

        return render(request, template, context)
    else:
        return redirect('blog:getblogs')


def editBlog(request, pk):
    template = 'blog/editblog.html'
    blog = get_object_or_404(Blog, pk=pk)
    form = BlogForm(request.POST or None, instance=blog)

    
    if form.is_valid():
        form.save()
        return redirect('blog:getblog', pk=pk)

    if (blog.user == request.user):
        context = {
            'form': form
        }

        return render(request, template, context)
    else:
        return redirect('blog:getblog', pk=pk)


def deleteBlog(request, pk):
    template = 'blog/deleteblog.html',
    blog = get_object_or_404(Blog, pk=pk)

    if blog.user == request.user:
        if request.method == 'POST':
            blog.delete()
            return redirect('blog:getblogs')
    else:
        return redirect('blog:getblog', pk=pk)

    context = {
        'blog': blog
    }

    return render(request, template, context)


def deleteComment(request, pk):
    comment = get_object_or_404(Comment, pk=pk)

    if (comment.user == request.user):
        comment.delete()
        return HttpResponseRedirect(reverse('blog:getblog', args=(comment.blog_id,)))
    else:
        return HttpResponseRedirect(reverse('blog:getblog', args=(comment.blog_id,)))
