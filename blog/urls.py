from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.getBlogs, name='getblogs'),
    path('blog/<int:pk>', views.getBlog, name='getblog'),
    path('create_blog/', views.createBlog, name='createblog'),
    path('edit_blog/<int:pk>', views.editBlog, name='editblog'),
    path('delete_blog/<int:pk>', views.deleteBlog, name='deleteblog'),
    path('delete_comment/<int:pk>', views.deleteComment, name='deletecomment'),
]
